import React, { useState } from 'react';
import { SearchBar } from './SearchBar'
import { ProductTable } from './ProductTable'


export const FilterableProductTable = ({ products }) => {

    const [searchBar, setSearchBar] = useState({ filterText: '', inStockOnly: false })

    const handleChange = ({ target: { name, type, value, checked } }) => {
        setSearchBar({
            ...searchBar,
            [name]: type === "number"
                ? Number(value)
                : type === "checkbox"
                    ? checked
                    : value
        });
    };

    return (
        <div>
            <SearchBar
                filterText={searchBar.filterText}
                inStockOnly={searchBar.inStockOnly}
                handleChange={handleChange} />
            <ProductTable products={products}
                filterText={searchBar.filterText}
                inStockOnly={searchBar.inStockOnly} />
        </div>
    );
}