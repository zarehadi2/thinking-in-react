import React from 'react';

export const SearchBar = ({ filterText, inStockOnly, handleChange }) => {

    return (
        <form>
            <input type="text" 
                name="filterText"
                value={filterText}
                onChange={handleChange}
                placeholder="Search..." /><br />
            <label>
                <input type="checkbox"
                    name="inStockOnly"
                    onChange={handleChange}
                    value={inStockOnly} />
                {' '} Only show products in stock
            </label>
        </form>
    );
}